<link href="css/custom/cubeportfolio.min.css" rel="stylesheet" />
<script src="css/custom/jquery.cubeportfolio.min.js"></script>
<script>
var gridContainer = jQuery('#js-grid-lightbox-gallery'),
        filtersContainer = $('#js-filters-lightbox-gallery2');

$(document).ready(function() {
    gridContainer.cubeportfolio({});

    <?php
    if(isset($_GET['sezione'])) {
        ?>
        console.log("scroll");
        var target = $('#works');
        if (target.length) {
            $('html, body').animate({
                scrollTop: target.offset().top + 100
            }, 1000, function() {
                console.log("end");


            });
        }
        <?php
    }
    ?>

    $('#js-grid-lightbox-gallery').on('initComplete.cbp',
        function() {
            console.log("init");

            filter('<?php echo $_GET['sezione']; ?>');
        }
    );


});

function scrollDown(section) {
    var hash = 'works';
    var target = $('#'+hash);
    if (target.length) {
        $('html, body').animate({
            scrollTop: target.offset().top
        }, 1000);
    }

    filter(section);
}

function filter(section) {
    var me = $("."+section);

    console.log(me);

    $(".all").removeClass('cbp-filter-item-active');
    $(".automotive").removeClass('cbp-filter-item-active');
    $(".calzaturiero").removeClass('cbp-filter-item-active');
    $(".elettrodomestici").removeClass('cbp-filter-item-active');
    $(".medicali").removeClass('cbp-filter-item-active');
    $(".articoli").removeClass('cbp-filter-item-active');
    $(".compositi").removeClass('cbp-filter-item-active');
    $(".moto").removeClass('cbp-filter-item-active');
    $(".parco").removeClass('cbp-filter-item-active');

    me.addClass('cbp-filter-item-active');

    $("#js-grid-lightbox-gallery").cubeportfolio('filter', me.data('filter'));
}
</script>
<section id="content">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="text-center">
                    <h3>Utilizziamo gli strumenti di <span class="highlight">progettazione e modellazione tridimensionale</span> più avanzati</h3>
                    <p>Curiamo lo sviluppo di ogni prodotto seguendo assieme al cliente l’evoluzione sin dalle prime fasi grazie all’ausilio dei propri sistemi<br />CAD-CAM POWER-SHAPE, POWER-MILL, SOLID WORKS.</p>
                    <p>Oltre alla costruzione di stampi, forniamo il servizio di prova stampo e produzioni presso ns. collaboratori.</p>
                    <p>Siamo specializzati nello sviluppo di stampi per i seguenti settori:</p>
                </div>
            </div>
        </div>
    </div>

    <div class="container">

    <div id="js-grid-awesome-work" class="cbp cbp-l-grid-work">
        <div class="cbp-item identity">
            <a href="javascript: scrollDown('automotive');" rel="nofollow">
                <div class="cbp-caption-defaultWrap">
                    <img src="img/automotive.jpg" alt="" >
                </div>
                <div class="cbp-caption-activeWrap"></div>
            </a>
            <a href="gallery" class="cbp-l-grid-work-title cbp-singlePage" rel="nofollow">Automotive</a>
            <div class="cbp-l-grid-work-desc">Web Design / Graphic</div>
        </div>
        <div class="cbp-item web-design logos">
            <a href="javascript: scrollDown('calzaturiero');" rel="nofollow">
                <div class="cbp-caption-defaultWrap">
                    <img src="img/calzaturiero.jpeg" alt="">
                </div>
                <div class="cbp-caption-activeWrap"></div>
            </a>
            <a href="gallery" class="cbp-l-grid-work-title cbp-singlePage" rel="nofollow">Calzaturiero</a>
            <div class="cbp-l-grid-work-desc">Logo / Web Design</div>
        </div>
        <div class="cbp-item graphic logos">
            <a href="javascript: scrollDown('elettrodomestici');" rel="nofollow">
                <div class="cbp-caption-defaultWrap">
                    <img src="img/elettrodomestici.jpg" alt="">
                </div>
                <div class="cbp-caption-activeWrap"></div>
            </a>
            <a href="gallery" class="cbp-l-grid-work-title cbp-singlePage" rel="nofollow">Elettrodomestici</a>
            <div class="cbp-l-grid-work-desc">Graphic / Logo</div>
        </div>
        <div class="cbp-item web-design graphic">
            <a href="javascript: scrollDown('medicali');" rel="nofollow">
                <div class="cbp-caption-defaultWrap">
                    <img src="img/medicale.jpg" alt="">
                </div>
                <div class="cbp-caption-activeWrap"></div>
            </a>
            <a href="gallery" class="cbp-l-grid-work-title cbp-singlePage" rel="nofollow">Medicali</a>
            <div class="cbp-l-grid-work-desc">Web Design / Graphic</div>
        </div>
        <div class="cbp-item identity web-design">
            <a href="javascript: scrollDown('articoli');"  rel="nofollow">
                <div class="cbp-caption-defaultWrap">
                    <img src="img/articoli.jpg" alt="">
                </div>
                <div class="cbp-caption-activeWrap"></div>
            </a>
            <a href="gallery" class="cbp-l-grid-work-title cbp-singlePage" rel="nofollow">Articoli Tecnici</a>
            <div class="cbp-l-grid-work-desc">Web Design / Identity</div>
        </div>
        <div class="cbp-item identity web-design">
            <a href="javascript: scrollDown('moto');" rel="nofollow">
                <div class="cbp-caption-defaultWrap">
                    <img src="img/moto.jpg" alt="">
                </div>
                <div class="cbp-caption-activeWrap"></div>
            </a>
            <a href="gallery" class="cbp-l-grid-work-title cbp-singlePage" rel="nofollow">Abbigliamento moto</a>
            <div class="cbp-l-grid-work-desc">Identity / Web Design</div>
        </div>
        <div style="clear: both;"></div>
    </div>
</div>

    <!-- divider -->
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="solidline">
                </div>
            </div>
        </div>
    </div>
    <!-- end divider -->

    <!-- parallax  -->
    <div id="parallax1" class="parallax text-light text-center marginbot50" data-stellar-background-ratio="0.5">
        <div class="container">
            <div class="row appear stats">
            </div>
        </div>
    </div>


    <!-- Portfolio Projects -->
    <div class="container marginbot50" id="works">
        <div class="row">
            <div class="col-lg-12">
                <h4 class="heading">Alcuni lavori</h4>

                <div class="clearfix">
                    <div id="js-filters-lightbox-gallery2" class="cbp-l-filters-button cbp-l-filters-left">
                        <div data-filter="*" class="cbp-filter-item-active cbp-filter-item all">Tutti<div class="cbp-filter-counter"></div></div>
                    <div data-filter=".automotive" class="cbp-filter-item automotive">Automotive<div class="cbp-filter-counter"></div></div>
                    <div data-filter=".calzaturiero" class="cbp-filter-item calzaturiero">Calzaturiero<div class="cbp-filter-counter"></div></div>
                    <div data-filter=".elettrodomestici" class="cbp-filter-item elettrodomestici">Elettrodomestici<div class="cbp-filter-counter"></div></div>
                    <div data-filter=".medicali" class="cbp-filter-item medicali">Medicali<div class="cbp-filter-counter"></div></div>
                    <div data-filter=".articoli" class="cbp-filter-item articoli">Articoli tecnici<div class="cbp-filter-counter"></div></div>
                    <div data-filter=".compositi" class="cbp-filter-item compositi">Materiali compositi<div class="cbp-filter-counter"></div></div>
                    <div data-filter=".moto" class="cbp-filter-item moto">Abbigliamento moto<div class="cbp-filter-counter"></div></div>
                    <div data-filter=".parco" class="cbp-filter-item parco">Parco macchine<div class="cbp-filter-counter"></div></div>
                    </div>
                </div>

                <div id="js-grid-lightbox-gallery" class="cbp">
                    <ul>
                        <li class="cbp-item automotive">
                            <div class="cbp-caption">
                                <div class="cbp-caption-defaultWrap">
                                    <img style="margin-bottom: 0px" src="img/gallery/automotive_1.jpg" alt="" class="gallery-thumb" />
                                </div>
                                <div class="cbp-caption-activeWrap">
                                    <div class="cbp-l-caption-alignCenter">
                                        <div class="cbp-l-caption-body">
                                            <a href="img/gallery/automotive_1.jpg" class="cbp-lightbox cbp-l-caption-buttonRight" data-title="">ingrandisci</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="cbp-l-grid-projects-title">Automotive</div>
                            <div class="cbp-l-grid-projects-desc">

                            </div>
                        </li>
                        <li class="cbp-item automotive">
                            <div class="cbp-caption">
                                <div class="cbp-caption-defaultWrap">
                                    <img style="margin-bottom: 0px" src="img/gallery/automotive_2.jpg" alt="" class="gallery-thumb" />
                                </div>
                                <div class="cbp-caption-activeWrap">
                                    <div class="cbp-l-caption-alignCenter">
                                        <div class="cbp-l-caption-body">
                                            <a href="img/gallery/automotive_2.jpg" class="cbp-lightbox cbp-l-caption-buttonRight" data-title="">ingrandisci</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="cbp-l-grid-projects-title">Automotive</div>
                            <div class="cbp-l-grid-projects-desc">

                            </div>
                        </li>
                        <li class="cbp-item automotive">
                            <div class="cbp-caption">
                                <div class="cbp-caption-defaultWrap">
                                    <img style="margin-bottom: 0px" src="img/gallery/automotive_3.jpg" alt="" class="gallery-thumb" />
                                </div>
                                <div class="cbp-caption-activeWrap">
                                    <div class="cbp-l-caption-alignCenter">
                                        <div class="cbp-l-caption-body">
                                            <a href="img/gallery/automotive_3.jpg" class="cbp-lightbox cbp-l-caption-buttonRight" data-title="">ingrandisci</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="cbp-l-grid-projects-title">Automotive</div>
                            <div class="cbp-l-grid-projects-desc">

                            </div>
                        </li>
                        <li class="cbp-item automotive">
                            <div class="cbp-caption">
                                <div class="cbp-caption-defaultWrap">
                                    <img style="margin-bottom: 0px" src="img/gallery/automotive_4.jpg" alt="" class="gallery-thumb" />
                                </div>
                                <div class="cbp-caption-activeWrap">
                                    <div class="cbp-l-caption-alignCenter">
                                        <div class="cbp-l-caption-body">
                                            <a href="img/gallery/automotive_4.jpg" class="cbp-lightbox cbp-l-caption-buttonRight" data-title="">ingrandisci</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="cbp-l-grid-projects-title">Automotive</div>
                            <div class="cbp-l-grid-projects-desc">

                            </div>
                        </li>
                        <li class="cbp-item automotive">
                            <div class="cbp-caption">
                                <div class="cbp-caption-defaultWrap">
                                    <img style="margin-bottom: 0px" src="img/gallery/automotive_5.jpg" alt="" class="gallery-thumb" />
                                </div>
                                <div class="cbp-caption-activeWrap">
                                    <div class="cbp-l-caption-alignCenter">
                                        <div class="cbp-l-caption-body">
                                            <a href="img/gallery/automotive_5.jpg" class="cbp-lightbox cbp-l-caption-buttonRight" data-title="">ingrandisci</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="cbp-l-grid-projects-title">Automotive</div>
                            <div class="cbp-l-grid-projects-desc">

                            </div>
                        </li>
                        <li class="cbp-item automotive">
                            <div class="cbp-caption">
                                <div class="cbp-caption-defaultWrap">
                                    <img style="margin-bottom: 0px" src="img/gallery/automotive_6.jpg" alt="" class="gallery-thumb" />
                                </div>
                                <div class="cbp-caption-activeWrap">
                                    <div class="cbp-l-caption-alignCenter">
                                        <div class="cbp-l-caption-body">
                                            <a href="img/gallery/automotive_6.jpg" class="cbp-lightbox cbp-l-caption-buttonRight" data-title="">ingrandisci</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="cbp-l-grid-projects-title">Automotive</div>
                            <div class="cbp-l-grid-projects-desc">

                            </div>
                        </li>
                        <li class="cbp-item automotive">
                            <div class="cbp-caption">
                                <div class="cbp-caption-defaultWrap">
                                    <img style="margin-bottom: 0px" src="img/gallery/automotive_7.jpg" alt="" class="gallery-thumb" />
                                </div>
                                <div class="cbp-caption-activeWrap">
                                    <div class="cbp-l-caption-alignCenter">
                                        <div class="cbp-l-caption-body">
                                            <a href="img/gallery/automotive_7.jpg" class="cbp-lightbox cbp-l-caption-buttonRight" data-title="">ingrandisci</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="cbp-l-grid-projects-title">Automotive</div>
                            <div class="cbp-l-grid-projects-desc">

                            </div>
                        </li>
                        <li class="cbp-item automotive">
                            <div class="cbp-caption">
                                <div class="cbp-caption-defaultWrap">
                                    <img style="margin-bottom: 0px" src="img/gallery/automotive_8.jpg" alt="" class="gallery-thumb" />
                                </div>
                                <div class="cbp-caption-activeWrap">
                                    <div class="cbp-l-caption-alignCenter">
                                        <div class="cbp-l-caption-body">
                                            <a href="img/gallery/automotive_8.jpg" class="cbp-lightbox cbp-l-caption-buttonRight" data-title="">ingrandisci</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="cbp-l-grid-projects-title">Automotive</div>
                            <div class="cbp-l-grid-projects-desc">

                            </div>
                        </li>
                        <li class="cbp-item automotive">
                            <div class="cbp-caption">
                                <div class="cbp-caption-defaultWrap">
                                    <img style="margin-bottom: 0px" src="img/gallery/automotive_9.jpg" alt="" class="gallery-thumb" />
                                </div>
                                <div class="cbp-caption-activeWrap">
                                    <div class="cbp-l-caption-alignCenter">
                                        <div class="cbp-l-caption-body">
                                            <a href="img/gallery/automotive_9.jpg" class="cbp-lightbox cbp-l-caption-buttonRight" data-title="">ingrandisci</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="cbp-l-grid-projects-title">Automotive</div>
                            <div class="cbp-l-grid-projects-desc">

                            </div>
                        </li>
                        <li class="cbp-item calzaturiero">
                            <div class="cbp-caption">
                                <div class="cbp-caption-defaultWrap">
                                    <img style="margin-bottom: 0px" src="img/gallery/calzaturiero_1.jpg" alt="" class="gallery-thumb" />
                                </div>
                                <div class="cbp-caption-activeWrap">
                                    <div class="cbp-l-caption-alignCenter">
                                        <div class="cbp-l-caption-body">
                                            <a href="img/gallery/calzaturiero_1.jpg" class="cbp-lightbox cbp-l-caption-buttonRight" data-title="">ingrandisci</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="cbp-l-grid-projects-title">Calzaturiero</div>
                            <div class="cbp-l-grid-projects-desc">

                            </div>
                        </li>
                        <li class="cbp-item calzaturiero">
                            <div class="cbp-caption">
                                <div class="cbp-caption-defaultWrap">
                                    <img style="margin-bottom: 0px" src="img/gallery/calzaturiero_2.jpg" alt="" class="gallery-thumb" />
                                </div>
                                <div class="cbp-caption-activeWrap">
                                    <div class="cbp-l-caption-alignCenter">
                                        <div class="cbp-l-caption-body">
                                            <a href="img/gallery/calzaturiero_2.jpg" class="cbp-lightbox cbp-l-caption-buttonRight" data-title="">ingrandisci</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="cbp-l-grid-projects-title">Calzaturiero</div>
                            <div class="cbp-l-grid-projects-desc">

                            </div>
                        </li>
                        <li class="cbp-item calzaturiero">
                            <div class="cbp-caption">
                                <div class="cbp-caption-defaultWrap">
                                    <img style="margin-bottom: 0px" src="img/gallery/calzaturiero_3.jpg" alt="" class="gallery-thumb" />
                                </div>
                                <div class="cbp-caption-activeWrap">
                                    <div class="cbp-l-caption-alignCenter">
                                        <div class="cbp-l-caption-body">
                                            <a href="img/gallery/calzaturiero_3.jpg" class="cbp-lightbox cbp-l-caption-buttonRight" data-title="">ingrandisci</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="cbp-l-grid-projects-title">Calzaturiero</div>
                            <div class="cbp-l-grid-projects-desc">

                            </div>
                        </li>
                        <li class="cbp-item calzaturiero">
                            <div class="cbp-caption">
                                <div class="cbp-caption-defaultWrap">
                                    <img style="margin-bottom: 0px" src="img/gallery/calzaturiero_4.jpg" alt="" class="gallery-thumb" />
                                </div>
                                <div class="cbp-caption-activeWrap">
                                    <div class="cbp-l-caption-alignCenter">
                                        <div class="cbp-l-caption-body">
                                            <a href="img/gallery/calzaturiero_4.jpg" class="cbp-lightbox cbp-l-caption-buttonRight" data-title="">ingrandisci</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="cbp-l-grid-projects-title">Calzaturiero</div>
                            <div class="cbp-l-grid-projects-desc">

                            </div>
                        </li>
                        <li class="cbp-item medicali">
                            <div class="cbp-caption">
                                <div class="cbp-caption-defaultWrap">
                                    <img style="margin-bottom: 0px" src="img/gallery/medicali_1.jpg" alt="" class="gallery-thumb" />
                                </div>
                                <div class="cbp-caption-activeWrap">
                                    <div class="cbp-l-caption-alignCenter">
                                        <div class="cbp-l-caption-body">
                                            <a href="img/gallery/medicali_1.jpg" class="cbp-lightbox cbp-l-caption-buttonRight" data-title="">ingrandisci</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="cbp-l-grid-projects-title">Medicali</div>
                            <div class="cbp-l-grid-projects-desc">

                            </div>
                        </li>
                        <li class="cbp-item elettrodomestici">
                            <div class="cbp-caption">
                                <div class="cbp-caption-defaultWrap">
                                    <img style="margin-bottom: 0px" src="img/gallery/elettrodomestici_1.jpg" alt="" class="gallery-thumb" />
                                </div>
                                <div class="cbp-caption-activeWrap">
                                    <div class="cbp-l-caption-alignCenter">
                                        <div class="cbp-l-caption-body">
                                            <a href="img/gallery/elettrodomestici_1.jpg" class="cbp-lightbox cbp-l-caption-buttonRight" data-title="">ingrandisci</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="cbp-l-grid-projects-title">Elettrodomestici</div>
                            <div class="cbp-l-grid-projects-desc">

                            </div>
                        </li>
                        <li class="cbp-item elettrodomestici">
                            <div class="cbp-caption">
                                <div class="cbp-caption-defaultWrap">
                                    <img style="margin-bottom: 0px" src="img/gallery/elettrodomestici_2.jpg" alt="" class="gallery-thumb" />
                                </div>
                                <div class="cbp-caption-activeWrap">
                                    <div class="cbp-l-caption-alignCenter">
                                        <div class="cbp-l-caption-body">
                                            <a href="img/gallery/elettrodomestici_2.jpg" class="cbp-lightbox cbp-l-caption-buttonRight" data-title="">ingrandisci</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="cbp-l-grid-projects-title">Elettrodomestici</div>
                            <div class="cbp-l-grid-projects-desc">

                            </div>
                        </li>
                        <li class="cbp-item elettrodomestici">
                            <div class="cbp-caption">
                                <div class="cbp-caption-defaultWrap">
                                    <img style="margin-bottom: 0px" src="img/gallery/elettrodomestici_3.jpg" alt="" class="gallery-thumb" />
                                </div>
                                <div class="cbp-caption-activeWrap">
                                    <div class="cbp-l-caption-alignCenter">
                                        <div class="cbp-l-caption-body">
                                            <a href="img/gallery/elettrodomestici_3.jpg" class="cbp-lightbox cbp-l-caption-buttonRight" data-title="">ingrandisci</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="cbp-l-grid-projects-title">Elettrodomestici</div>
                            <div class="cbp-l-grid-projects-desc">

                            </div>
                        </li>
                        <li class="cbp-item articoli">
                            <div class="cbp-caption">
                                <div class="cbp-caption-defaultWrap">
                                    <img style="margin-bottom: 0px" src="img/gallery/articoli_tecnici_1.jpg" alt="" class="gallery-thumb" />
                                </div>
                                <div class="cbp-caption-activeWrap">
                                    <div class="cbp-l-caption-alignCenter">
                                        <div class="cbp-l-caption-body">
                                            <a href="img/gallery/articoli_tecnici_1.jpg" class="cbp-lightbox cbp-l-caption-buttonRight" data-title="">ingrandisci</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="cbp-l-grid-projects-title">Articoli tecnici</div>
                            <div class="cbp-l-grid-projects-desc">

                            </div>
                        </li>
                        <li class="cbp-item articoli">
                            <div class="cbp-caption">
                                <div class="cbp-caption-defaultWrap">
                                    <img style="margin-bottom: 0px" src="img/gallery/articoli_tecnici_3.jpg" alt="" class="gallery-thumb" />
                                </div>
                                <div class="cbp-caption-activeWrap">
                                    <div class="cbp-l-caption-alignCenter">
                                        <div class="cbp-l-caption-body">
                                            <a href="img/gallery/articoli_tecnici_3.jpg" class="cbp-lightbox cbp-l-caption-buttonRight" data-title="">ingrandisci</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="cbp-l-grid-projects-title">Articoli tecnici</div>
                            <div class="cbp-l-grid-projects-desc">

                            </div>
                        </li>
                        <li class="cbp-item articoli">
                            <div class="cbp-caption">
                                <div class="cbp-caption-defaultWrap">
                                    <img style="margin-bottom: 0px" src="img/gallery/articoli_tecnici_4.jpg" alt="" class="gallery-thumb" />
                                </div>
                                <div class="cbp-caption-activeWrap">
                                    <div class="cbp-l-caption-alignCenter">
                                        <div class="cbp-l-caption-body">
                                            <a href="img/gallery/articoli_tecnici_4.jpg" class="cbp-lightbox cbp-l-caption-buttonRight" data-title="">ingrandisci</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="cbp-l-grid-projects-title">Articoli tecnici</div>
                            <div class="cbp-l-grid-projects-desc">

                            </div>
                        </li>
                        <li class="cbp-item articoli">
                            <div class="cbp-caption">
                                <div class="cbp-caption-defaultWrap">
                                    <img style="margin-bottom: 0px" src="img/gallery/articoli_tecnici_5.jpg" alt="" class="gallery-thumb" />
                                </div>
                                <div class="cbp-caption-activeWrap">
                                    <div class="cbp-l-caption-alignCenter">
                                        <div class="cbp-l-caption-body">
                                            <a href="img/gallery/articoli_tecnici_5.jpg" class="cbp-lightbox cbp-l-caption-buttonRight" data-title="">ingrandisci</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="cbp-l-grid-projects-title">Articoli tecnici</div>
                            <div class="cbp-l-grid-projects-desc">

                            </div>
                        </li>
                        <li class="cbp-item compositi">
                            <div class="cbp-caption">
                                <div class="cbp-caption-defaultWrap">
                                    <img style="margin-bottom: 0px" src="img/gallery/materiali_compositi_1.jpg" alt="" class="gallery-thumb" />
                                </div>
                                <div class="cbp-caption-activeWrap">
                                    <div class="cbp-l-caption-alignCenter">
                                        <div class="cbp-l-caption-body">
                                            <a href="img/gallery/materiali_compositi_1.jpg" class="cbp-lightbox cbp-l-caption-buttonRight" data-title="">ingrandisci</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="cbp-l-grid-projects-title">Materiali compositi</div>
                            <div class="cbp-l-grid-projects-desc">

                            </div>
                        </li>
                        <li class="cbp-item parco">
                            <div class="cbp-caption">
                                <div class="cbp-caption-defaultWrap">
                                    <img style="margin-bottom: 0px" src="img/gallery/parco_macchine_1.jpg" alt="" class="gallery-thumb" />
                                </div>
                                <div class="cbp-caption-activeWrap">
                                    <div class="cbp-l-caption-alignCenter">
                                        <div class="cbp-l-caption-body">
                                            <a href="img/gallery/parco_macchine_1.jpg" class="cbp-lightbox cbp-l-caption-buttonRight" data-title="">ingrandisci</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="cbp-l-grid-projects-title">Parco macchine</div>
                            <div class="cbp-l-grid-projects-desc">

                            </div>
                        </li>
                        <li class="cbp-item parco">
                            <div class="cbp-caption">
                                <div class="cbp-caption-defaultWrap">
                                    <img style="margin-bottom: 0px" src="img/gallery/parco_macchine_2.jpg" alt="" class="gallery-thumb" />
                                </div>
                                <div class="cbp-caption-activeWrap">
                                    <div class="cbp-l-caption-alignCenter">
                                        <div class="cbp-l-caption-body">
                                            <a href="img/gallery/parco_macchine_2.jpg" class="cbp-lightbox cbp-l-caption-buttonRight" data-title="">ingrandisci</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="cbp-l-grid-projects-title">Parco macchine</div>
                            <div class="cbp-l-grid-projects-desc">

                            </div>
                        </li>
                        <li class="cbp-item parco">
                            <div class="cbp-caption">
                                <div class="cbp-caption-defaultWrap">
                                    <img style="margin-bottom: 0px" src="img/gallery/parco_macchine_3.jpg" alt="" class="gallery-thumb" />
                                </div>
                                <div class="cbp-caption-activeWrap">
                                    <div class="cbp-l-caption-alignCenter">
                                        <div class="cbp-l-caption-body">
                                            <a href="img/gallery/parco_macchine_3.jpg" class="cbp-lightbox cbp-l-caption-buttonRight" data-title="">ingrandisci</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="cbp-l-grid-projects-title">Parco macchine</div>
                            <div class="cbp-l-grid-projects-desc">

                            </div>
                        </li>
                        <li class="cbp-item parco">
                            <div class="cbp-caption">
                                <div class="cbp-caption-defaultWrap">
                                    <img style="margin-bottom: 0px" src="img/gallery/parco_macchine_5.jpg" alt="" class="gallery-thumb" />
                                </div>
                                <div class="cbp-caption-activeWrap">
                                    <div class="cbp-l-caption-alignCenter">
                                        <div class="cbp-l-caption-body">
                                            <a href="img/gallery/parco_macchine_5.jpg" class="cbp-lightbox cbp-l-caption-buttonRight" data-title="">ingrandisci</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="cbp-l-grid-projects-title">Parco macchine</div>
                            <div class="cbp-l-grid-projects-desc">

                            </div>
                        </li>
                        <li class="cbp-item parco">
                            <div class="cbp-caption">
                                <div class="cbp-caption-defaultWrap">
                                    <img style="margin-bottom: 0px" src="img/gallery/parco_macchine_6.jpg" alt="" class="gallery-thumb" />
                                </div>
                                <div class="cbp-caption-activeWrap">
                                    <div class="cbp-l-caption-alignCenter">
                                        <div class="cbp-l-caption-body">
                                            <a href="img/gallery/parco_macchine_6.jpg" class="cbp-lightbox cbp-l-caption-buttonRight" data-title="">ingrandisci</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="cbp-l-grid-projects-title">Parco macchine</div>
                            <div class="cbp-l-grid-projects-desc">

                            </div>
                        </li>
                        <li class="cbp-item parco">
                            <div class="cbp-caption">
                                <div class="cbp-caption-defaultWrap">
                                    <img style="margin-bottom: 0px" src="img/gallery/parco_macchine_7.jpg" alt="" class="gallery-thumb" />
                                </div>
                                <div class="cbp-caption-activeWrap">
                                    <div class="cbp-l-caption-alignCenter">
                                        <div class="cbp-l-caption-body">
                                            <a href="img/gallery/parco_macchine_7.jpg" class="cbp-lightbox cbp-l-caption-buttonRight" data-title="">ingrandisci</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="cbp-l-grid-projects-title">Parco macchine</div>
                            <div class="cbp-l-grid-projects-desc">

                            </div>
                        </li>

                        <li class="cbp-item moto">
                            <div class="cbp-caption">
                                <div class="cbp-caption-defaultWrap">
                                    <img style="margin-bottom: 0px" src="img/gallery/moto_1.jpg" alt="" class="gallery-thumb" />
                                </div>
                                <div class="cbp-caption-activeWrap">
                                    <div class="cbp-l-caption-alignCenter">
                                        <div class="cbp-l-caption-body">
                                            <a href="img/gallery/moto_1.jpg" class="cbp-lightbox cbp-l-caption-buttonRight" data-title="">ingrandisci</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="cbp-l-grid-projects-title">Abbigliamento moto</div>
                            <div class="cbp-l-grid-projects-desc">

                            </div>
                        </li>
                        <li class="cbp-item moto">
                            <div class="cbp-caption">
                                <div class="cbp-caption-defaultWrap">
                                    <img style="margin-bottom: 0px" src="img/gallery/moto_2.jpg" alt="" class="gallery-thumb" />
                                </div>
                                <div class="cbp-caption-activeWrap">
                                    <div class="cbp-l-caption-alignCenter">
                                        <div class="cbp-l-caption-body">
                                            <a href="img/gallery/moto_2.jpg" class="cbp-lightbox cbp-l-caption-buttonRight" data-title="">ingrandisci</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="cbp-l-grid-projects-title">Abbigliamento moto</div>
                            <div class="cbp-l-grid-projects-desc">

                            </div>
                        </li>
                        <li class="cbp-item moto">
                            <div class="cbp-caption">
                                <div class="cbp-caption-defaultWrap">
                                    <img style="margin-bottom: 0px" src="img/gallery/moto_3.jpg" alt="" class="gallery-thumb" />
                                </div>
                                <div class="cbp-caption-activeWrap">
                                    <div class="cbp-l-caption-alignCenter">
                                        <div class="cbp-l-caption-body">
                                            <a href="img/gallery/moto_3.jpg" class="cbp-lightbox cbp-l-caption-buttonRight" data-title="">ingrandisci</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="cbp-l-grid-projects-title">Abbigliamento moto</div>
                            <div class="cbp-l-grid-projects-desc">

                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>


    <!-- divider -->
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="solidline">
                </div>
            </div>
        </div>
    </div>
    <!-- end divider -->


</section>
