<section id="featured" class="bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <!-- Slider -->
                <div id="main-slider" class="main-slider flexslider">
                    <ul class="slides">
                        <li>
                            <img src="img/slides/flexslider/1.jpg" alt="" />
                            <div class="flex-caption">
                                <p>Ci occupiamo della progettazione e la realizzazione di stampi</p> 
                                <a href="servizi" class="btn btn-theme">Vedi tutti i servizi</a>
                            </div>
                        </li>
                        <li>
                            <img src="img/slides/flexslider/4.jpg" alt="" />
                            <div class="flex-caption">
                                <h3>Reverse Engineering</h3> 
                                <p>Disponiamo del sistema Reverse Engineering per la ricostruzione di superfici matematiche da una nuvola di punti.</p> 
                                <a href="servizi" class="btn btn-theme">Vedi tutti i servizi</a>
                            </div>
                        </li>
                        <li>
                            <img src="img/slides/flexslider/3.jpg" alt="" />
                            <div class="flex-caption">
                                <h3>Esperienza nel settore</h3> 
                                <p></p> 
                                <a href="servizi" class="btn btn-theme">Vedi tutti i servizi</a>
                            </div>
                        </li>
                    </ul>
                </div>
                <!-- end slider -->
            </div>
        </div>
    </div>
</section>
<section id="content" style="padding-top: 0px;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h3>L'azienda</h3>
                <p>
                    NEL 1988 I fratelli <strong>Aldo e Ilario Zardin</strong> da una precedente esperienza fondano la <strong>ZETA DUE</strong>, azienda italiana <strong>specializzata nella costruzione e modifica stampi</strong> nel settore calzaturiero e in particolare per scarponi da sci con l’utilizzo di macchine manuali.</p>

                <p>Dopo aver consolidato la propria esperienza la <strong>ZETA DUE</strong> si rivolge a un mercato più ampio interpretando al meglio l’esigenza dei propri clienti.</p>


                <p>Rivolge particolare attenzione all’<strong>innovazione tecnologica</strong> con l’utilizzo di macchinari 3 - 5 assi e con l’ausilio degli strumenti più avanzati di <strong>progettazione e modellazione tridimensionale</strong> concretizza al meglio l’esperienza di tutto lo staff aziendale.<br />
Cura lo sviluppo  del prodotto seguendo assieme al cliente l’evoluzione sin dalle prime fasi grazie all’ausilio dei propri sistemi <strong>CAD-CAM
		POWER-SHAPE, 
		POWER-MILL, 
                    SOLID WORKS</strong>.</p>

                <p>Dispone inoltre del sistema <strong>REVERSE-ENGINEERING</strong> per la ricostruzione di superfici matematiche da una nuvola di punti.
                </p>
            </div>
        </div>

    </div>
</section>
