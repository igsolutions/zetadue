<section id="inner-headline">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <ul class="breadcrumb">
                    <li><a href="#"><i class="fa fa-home"></i></a><i class="icon-angle-right"></i></li>
                    <li class="active">Contatti</li>
                </ul>
            </div>
        </div>
    </div>
</section>
<section id="content">
    <div class="map">
        <div id="google-map" data-latitude="45.7826649" data-longitude="11.9909863"></div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <h2>Contattaci <small>mettiti in contatto con noi compilando il form sottostante</small></h2>
                <hr class="colorgraph">
                <div id="sendmessage">Il tuo messaggio è stato inviato! Grazie!</div>
                <div id="errormessage"></div>
                <form action="" method="post" role="form" id="contact_form" class="contactForm">
                    <div class="form-group">
                        <input type="text" name="name" class="form-control" id="name" placeholder="Il tuo nome" data-rule="minlen:2" data-msg="Il nome deve avere almeno 4 caratteri" required />
                        <div class="validation"></div>
                    </div>
                    <div class="form-group">
                        <input type="email" class="form-control" name="email" id="email" placeholder="Il tuo indirizzo e-mail" data-rule="email" data-msg="Inserisci un indirizzo e-mail valido" required />
                        <div class="validation"></div>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="subject" id="subject" placeholder="Oggetto" data-rule="minlen:4" data-msg="L'oggetto deve avere almeno 4 caratteri" required />
                        <div class="validation"></div>
                    </div>
                    <div class="form-group">
                        <textarea class="form-control" name="message" id="message" rows="5" data-rule="required" data-msg="Il campo messaggio è obbligatorio" placeholder="Messaggio" required></textarea>
                        <div class="validation"></div>
                    </div>

                    <div class="form-group">
                        <input type="checkbox" name="privacy" name="privacy" id="privacy" required />
                        <label for="privacy">Ho letto e accetto la <a href="privacy.pdf" target="_blank">Privacy policy</a></label>
                    </div>

                    <div class="text-center"><button type="submit" class="btn btn-theme btn-block btn-md">Invia messaggio</button></div>
                    <br /><br /><div class="text-center" id="msg_contact"></div>
                </form>
                <hr class="colorgraph">

            </div>
        </div>
    </div>
</section>
<script src="https://maps.google.com/maps/api/js?sensor=true&key=AIzaSyAduwLqtXQlH5M9t6QR1tGbuPn9sf4bmJw"></script>
<script>
jQuery(document).ready(function( $ ) {

    //Google Map
    var get_latitude = $('#google-map').data('latitude');
    var get_longitude = $('#google-map').data('longitude');

    function initialize_google_map() {
        var myLatlng = new google.maps.LatLng(get_latitude, get_longitude);
        var mapOptions = {
            zoom: 14,
            scrollwheel: false,
            center: myLatlng
        };
        var map = new google.maps.Map(document.getElementById('google-map'), mapOptions);
        var marker = new google.maps.Marker({
            position: myLatlng,
            map: map
        });
    }
    google.maps.event.addDomListener(window, 'load', initialize_google_map);

    $("#contact_form").submit(function() {
        var params = {
            name: $("#name").val(),
            email: $("#email").val(),
            subject: $("#subject").val(),
            message: $("#message").val()
        };

        $.post("core/contact.php", params, function(data) {
            console.log("apiPost success. Endpoint: contact - JSON: " + JSON.stringify(data));

            $("#msg_contact").html(data);

            return false;
        }).fail(function() {

        });

        return false;
    });
});
</script>
