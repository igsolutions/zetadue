<link href="css/cubeportfolio.min.css" rel="stylesheet" />
<section id="inner-headline">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <ul class="breadcrumb">
                    <li><a href="#"><i class="fa fa-home"></i></a><i class="icon-angle-right"></i></li>
                    <li class="active">Gallery</li>
                </ul>
            </div>
        </div>
    </div>
</section>
<section id="content">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h4 class="heading">Foto Gallery</h4>

                <div id="grid-container" class="cbp-l-grid-projects">
                    <ul>
                        <?php
                        for($i = 0; $i <= 32; $i++) {
                            ?>
                        <li class="cbp-item">
                            <div class="cbp-caption">
                                <div class="cbp-caption-defaultWrap">
                                    <img src="img/gallery/ga/gallery_<?php echo $i; ?>.jpg" alt="" class="gallery-thumb" />
                                </div>
                                <div class="cbp-caption-activeWrap">
                                    <div class="cbp-l-caption-alignCenter">
                                        <div class="cbp-l-caption-body">
                                            <a href="img/gallery/ga/gallery_<?php echo $i; ?>.jpg" class="cbp-lightbox cbp-l-caption-buttonRight" data-title="">ingrandisci</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="cbp-l-grid-projects-title"></div>
                            <div class="cbp-l-grid-projects-desc">

                            </div>
                        </li>
                        <?php
                        }
                        ?>
                    </ul>
                </div>

            </div>
        </div>
    </div>
</section>